import angular from 'angular';
import 'angular-ui-router';
import './app/modules/items/itemsModule';
import tableView from './app/partials/tableView.html';
import tileView from './app/partials/tileView.html';
import addItemView from './app/partials/addItemView.html';
import headerDirective from './app/directives/header/headerDirective';
import toolBarDirective from './app/directives/toolBar/toolBarDirective';
import './app/css/style.css';

const App = angular.module( 'App', [ 'ui.router', 'itemsModule' ] );
App.directive( 'header', headerDirective );
App.directive( 'toolbar', toolBarDirective );

App.config( ( $stateProvider, $urlRouterProvider, $locationProvider, $urlMatcherFactoryProvider ) => {
    $urlMatcherFactoryProvider.strictMode( false );
    $stateProvider
        .state( 'tableView', {
            url: '/table',
            template: tableView,
            controller: 'itemsCtrl',
            controllerAs: 'ctrl'
        } ).state( 'tileView', {
            url: '/tile',
            template: tileView,
            controller: 'itemsCtrl',
            controllerAs: 'ctrl'
        } ).state( 'editItem', {
            url: '/edit?:id',
            template: addItemView,
            controller: 'addItemCtrl',
            controllerAs: 'ctrl'
        } );
    $urlRouterProvider.otherwise('/table');
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });
});
