module.exports = {
    entry : './app.js',
    output : {
        filename : './bundle.js'
    },
    module: {
       loaders: [
			{test: /.jsx?$/, loader: 'babel-loader',exclude: [/node_modules/,/bower_components/], query: {presets: ['es2015']}},
			{ test: /\.css$/, loader: 'style-loader!css-loader' },
			{ test: /\.html$/, loader: "html-loader", exclude: [/node_modules/,/bower_components/]},
			{ test: /\.js$/, loader: "eslint-loader", exclude: [/node_modules/, /bower_components/]},

       ]
    },
    watch : true,
    devtool : 'cheap-module-eval-source-map',
    devServer: {
        port: 8080,
        historyApiFallback: true
    }
}
