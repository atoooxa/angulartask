import headerTemplate from './headerTemplate.html';
export default ( $rootScope ) => {
    return {
        template: headerTemplate,
        replace: true,
        link: ( scope ) => {
            $rootScope.$on( '$stateChangeSuccess', ( event, toState ) => {
                scope.state = toState.name;
            } );
        }
    }
}
