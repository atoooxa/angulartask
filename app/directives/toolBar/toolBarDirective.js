import toolBarTemplate from './toolBarTemplate.html';
export default () => {
    return {
        scope: {
            preItemsFilter: '=',
            itemsPerPage: '=',
            page: '=',
            filterIt: '&'
        },
        replace: true,
        template: toolBarTemplate
    }
}
