export default ( itemsService ) => {
    return {
        scope: {
            imgSrc: '='
        },
        link: ( scope, elem, attrs ) => {
            elem.on( 'dragover', event => {
                event.preventDefault();
            } );
            elem.on( 'drop', ( event ) => {
                event.preventDefault();
                itemsService.fileReader( event ).then( ( data ) => {
                    scope.imgSrc = data;
                } );
            } );
        }
    }
}
