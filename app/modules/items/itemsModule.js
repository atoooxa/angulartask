import angular from 'angular';
import itemsCtrl from './controllers/itemsCtrl';
import addItemCtrl from './controllers/addItemCtrl';
import itemsService from './services/itemsService';
import pageFilter from './filters/pageFilter';
import dropFile from './directives/dropFile';

const itemsModule = angular.module( 'itemsModule', [] );
itemsModule.controller( 'itemsCtrl', itemsCtrl );
itemsModule.controller( 'addItemCtrl', addItemCtrl );
itemsModule.service( 'itemsService', itemsService );
itemsModule.filter( 'pageFilter', pageFilter );
itemsModule.directive( 'dropfile', dropFile );

export default itemsModule;
