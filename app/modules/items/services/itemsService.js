import _ from 'lodash';

class itemsService {
    constructor( $window, $q ) {
        this.localStorage = $window.localStorage;
        this.$q = $q;
    }

    getItemsData() {
        const items = this.localStorage.getItem('items') || '[]';
        return this.$q.resolve( JSON.parse( items ) );
    }

    saveItem( data ) {
        const items = JSON.parse( this.localStorage.getItem('items') ) || [];
        let id = data.id;
        if ( id !== undefined ) {
            const item = _.findIndex( items, { id } );
            if ( ~item ) items[item] = data;
        } else {
            id = JSON.parse( this.localStorage.getItem('id') ) || 0;
            data.id = id;
            items.push( data );
            this.localStorage.setItem( id, id + 1 );
        }
        this.localStorage.setItem( 'items', JSON.stringify( items ) );
        return this.$q.resolve();
    }

    removeItem( data ) {
        const items = JSON.parse( this.localStorage.getItem('items') ) || [];
        const item = _.findIndex( items, { id: data.id } );
        if ( ~item ) items.splice( item, 1 );
        this.localStorage.setItem( 'items', JSON.stringify( items ) );
        return this.$q.resolve();
    }

    itemInfo( id ) {
        const items = JSON.parse( this.localStorage.getItem('items') ) || '[]';
        const item = _.findIndex( items, { id } );
        if ( ~item ) return this.$q.resolve( items[item] );
    }

    fileReader( event ) {
        const reader = new FileReader();
        return this.$q( ( resolve ) => {
            reader.onload = ( event ) => {
                resolve( event.target.result );
            }
            reader.readAsDataURL( event.dataTransfer.files[0] );
        });
    }
}
itemsService.$injector = [ '$q', '$window' ];
export default itemsService;
