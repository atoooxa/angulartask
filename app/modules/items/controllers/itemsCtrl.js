class itemsCtrl {
    constructor( $scope, itemsService ) {
        itemsService.getItemsData().then( ( data )  => {
            this.items = data;
            this.itemsPerPage = '10';
            this.page = '1';
        } );
    }

    filterIt() {
        this.itemsContentFilter = this.preItemsFilter;
    }
}
export default itemsCtrl;
