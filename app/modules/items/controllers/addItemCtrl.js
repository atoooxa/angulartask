class addItemCtrl {
    constructor( itemsService, $state, $stateParams ) {
        this.itemsService = itemsService;
        this.$state = $state;
        this.id = Number( $stateParams.id );
        this.product = {};

        if ( this.id ) {
            itemsService.itemInfo( this.id ).then( ( data ) => {
                this.product = data;
            } );
        }
    }

    saveItem() {
        this.itemsService.saveItem( this.product ).then( ( data ) => {
            this.$state.go('tableView');
        } );
    }

    removeItem() {
        this.itemsService.removeItem( this.product ).then( ( data ) => {
            this.$state.go('tableView');
        } );
    }
}
export default addItemCtrl;
