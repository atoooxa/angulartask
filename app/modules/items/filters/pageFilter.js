export default () => {
    return ( items, page, itemsPerPage ) => {
        return items.slice( (page - 1) * itemsPerPage, page * itemsPerPage );
    }
}
